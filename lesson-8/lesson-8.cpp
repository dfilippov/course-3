﻿#include <iostream>
#include <gtest/gtest.h>
#include "tests.h"

int main()
{
	testing::InitGoogleTest();
	return RUN_ALL_TESTS();
}