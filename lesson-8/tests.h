#pragma once
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>
#include "Date.h"

using namespace std;


template <typename T>
T min_el(vector<T> v) {
    return *min_element(begin(v), end(v));
}
template <typename T>
T max_el(vector<T> v) {
    return *max_element(begin(v), end(v));
}

TEST(minimum, min_el) {
    vector<int> v1{ 4,2,7,3,-5,0,-3,1 };
    ASSERT_EQ(-5, min_el(v1));
    vector<double> v2{ 4.21, 0.42, 0.4242, 3.13 };
    ASSERT_NEAR(0.42, min_el(v2), 1E-5);
}
TEST(maximum, max_el) {
    vector<int> v1{ 4,2,7,3,-5,0,-3,1 };
    ASSERT_TRUE(7 == max_el(v1));
    vector<double> v2{ 4.21, 0.42, 0.4242, 3.13 };
    ASSERT_DOUBLE_EQ(max_el(v2), 4.21);
}


// Tests for the Date class using a "fixation":

class TestDate : public testing::Test {
protected:
    void SetUp() override {
        date = new Date(4, 7, 2021);
    }
    void TearDown() override {
        delete date;
    }
    Date* date;
};

TEST_F(TestDate, get_methods) {
    ASSERT_EQ(date->day(), 4);
    ASSERT_EQ(date->month(), 7);
    ASSERT_EQ(date->year(), 2021);
}
TEST_F(TestDate, set_methods) {
    date->set_day(10);
    ASSERT_EQ(date->day(), 10);
    date->set_month(9);
    ASSERT_EQ(date->month(), 9);
    date->set_year(2022);
    ASSERT_EQ(date->year(), 2022);
}
TEST_F(TestDate, print) {
    ASSERT_STREQ(date->print().c_str(), "4.7.2021") << "you'll see this in case of an error";
}