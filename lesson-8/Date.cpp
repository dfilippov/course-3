#include <iostream>
#include <string>
#include <sstream>
#include "Date.h"


Date::Date(int d, int m, int y)
	: m_day(d), m_month(m), m_year(y) {}

int Date::day() const
{
	return m_day;
}

int Date::month() const
{
	return m_month;
}

int Date::year() const
{
	return m_year;
}

string Date::print() const
{
	ostringstream os;
	os << m_day << "." << m_month << "." << m_year;
	return os.str();
}

void Date::set_day(int d)
{
	m_day = d;
}

void Date::set_month(int m)
{
	m_month = m;
}

void Date::set_year(int y)
{
	m_year = y;
}