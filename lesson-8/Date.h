#pragma once
#include <string>

using namespace std;

class Date {
private:
    int m_day, m_month, m_year;
public:
    Date() = default;
    Date(int d, int m, int y);
    int day() const;
    int month() const;
    int year() const;
    string print() const;
    void set_day(int d);
    void set_month(int m);
    void set_year(int y);
};

