#pragma once
#include <mutex>
#include <functional>

namespace task1_another_variant_namespace
{
	class CoutWrapper
	{
	private:
		std::mutex coutMutex;
		std::function<void(int)> lambda = [](int number) {};
	public:

		template <typename F>
		CoutWrapper& bind(F &l)
		{
			lambda = l;
			return *this;
		}

		// TODO: deal with lambdas without arguments
		// void operator() ()
		// {
		// 	 std::lock_guard lg(coutMutex);
		//   lambda();
		// }

		template <typename ... Args>
		void operator() (Args ...rest)
		{
			std::lock_guard lg(coutMutex);
			lambda(rest...);
		}

	};

	CoutWrapper pcout;
}