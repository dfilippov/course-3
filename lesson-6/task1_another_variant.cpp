#include <iostream>
#include "task1_another_variant.h"
#include "task1_another_variant_namespace.h"

using namespace std;
using namespace task1_another_variant_namespace;


// Define a race-condition-safe wrapper for the std::cout object named pcout.
// It should work with several threads and output data to console. Give an
// example of usage.
void task1_another_variant()
{
	// An asynchronous function for the first task. Will be called in separated 
	// thread.
	auto task1_print = [](int number)
	{
		cout << "Thread #" << number << " started working." << endl;
		this_thread::sleep_for(1s);
		cout << "Thread #" << number << " exited." << endl;
	};

	cout << "Task 1 (another implementation)" << endl;
	cout << "Variant with std::cout (race condition):" << endl;
	thread th1rc(task1_print, 1);
	thread th2rc(task1_print, 2);
	thread th3rc(task1_print, 3);
	th1rc.join();
	th2rc.join();
	th3rc.join();
	cout << endl;
	cout << "Variant with pcout (without race condition):" << endl;
	// FIXME: fix a compilation error:
	// thread th1(pcout.bind(task1_print), 1);
	// thread th2(pcout.bind(task1_print), 2);
	// thread th3(pcout.bind(task1_print), 3);
	// th1.join();
	// th2.join();
	// th3.join();
}