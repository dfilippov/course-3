#include <iostream>
#include <thread>
#include <mutex>
#include <math.h>
#include "task2.h"

using namespace std;

// TODO: rewrite with the usage of condition_variable

mutex progress_mutex;


// Checks whether the given number is prime.
bool isPrime(const int& number)
{
	if (number == 2)
	{
		return true;
	}
	else if
	(
		(number == 0 || number == 1) ||
		(number % 2 == 0)
	)
	{
		return false;
	}

	for (int i = 3; i < number; i += 2)
	{
		if (number % i == 0)
		{
			return false;
		}
	}
	return true;
}


// Finds the i-th prime number. Saves the progress of calculation (the current 
// number of calculated prime numbers) in the second argument. Dumb algorithm.
int findIPrime(const int i, int& progress)
{
	long long number = 2;

	do
	{
		if (isPrime(number))
		{
			progress_mutex.lock();
			progress++;
			progress_mutex.unlock();
		}
		number++;
	} while (progress < i);
	cout << "Done!" << endl;
	return --number;
}


// An auxiliary function for displaying the prime numbers calculation progress.
void printPrimeCalcProgress(const int i, int& progress)
{
	progress_mutex.lock();
	cout << "Calculating the " << i << "-th prime number:" << endl;
	progress_mutex.unlock();

	while (progress < i) {
		progress_mutex.lock();
		cout << round(static_cast<double>(progress) / i * 100) << "% done..." << endl;
		progress_mutex.unlock();
		this_thread::sleep_for(1s);
	}
}


// Define a function, which calculates the i-th prime number. The calculations
// should take place in a separate thread. Output the progress of calculation
// to console.
void task2()
{
	cout << "Task 2." << endl;
	const int i = 50'000;
	int progress = 0;
	int result = 0;

	thread calc([&](int& p) { result = findIPrime(i, p); }, ref(progress));
	thread print(printPrimeCalcProgress, i, ref(progress));
	calc.detach();
	print.join();
	cout << "Result: " << result << endl;
}
