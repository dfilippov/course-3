#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <algorithm>
#include <random>
#include "task3.h"

using namespace std;

// TODO: rewrite with the usage of condition_variable

mutex action_mutex;


void thief
(
	map<string, int> &thingsAtHome, 
	const vector<pair<string, double>> &sortedThingsAvailable
)
{
	lock_guard lg(action_mutex);
	string thingName;

	for (auto it = sortedThingsAvailable.begin(); it != sortedThingsAvailable.end(); it++)
	{
		thingName = (*it).first;
		if (thingsAtHome.count(thingName) > 0 && thingsAtHome[thingName] > 0)
		{
			cout << "A thief stole the " << thingName << "!" << endl;
			thingsAtHome[thingName] -= 1;
			break;
		}
	}
}

void owner
(
	map<string, int> &thingsAtHome, 
	const vector<pair<string, double>> &thingsAvailable,
	const uniform_int_distribution<> &distribution,
	mt19937 &generator
)
{
	lock_guard lg(action_mutex);
	string thingName = thingsAvailable[distribution(generator)].first;
	cout << "Owner brings the " << thingName << "." << endl;
	thingsAtHome[thingName]++;
}

// Model a situation: there are two people: owner and thief. The former brings
// things to the home, the latter always takes a thing with a maximum value.
void task3()
{
	cout << "Task 3." << endl;
	random_device rd;
	mt19937 generator(rd());

	vector<pair<string, double>> thingsAvailable = {
		{ "Robot vacuum", 13'110 },
		{ "Smart gateway", 1'873.44 },
		{ "Mini computer", 4'686.18 },
		{ "Table lamp", 2'993.16 },
		{ "LED strip", 1'668.15 },
		{ "Foldable table", 1'290.52 },
	};

	uniform_int_distribution<> distribution(0, thingsAvailable.size());

	map<string, int> thingsAtHome = {
		{ "Robot vacuum", 1 },
		{ "Table lamp", 1 },
		{ "Foldable table", 1 },
	};

	cout << "Available things:" << endl;
	sort(thingsAvailable.begin(), thingsAvailable.end(), [](auto first, auto second) {
		return first.second > second.second;
	});
	auto print = [](auto item) {
		cout << item.first << ":\t" << item.second << endl;
	};
	for_each(thingsAvailable.begin(), thingsAvailable.end(), print);
	cout << endl;


	cout << "Initial state of things at home:" << endl;
	for_each(thingsAtHome.begin(), thingsAtHome.end(), print);
	cout << endl;

	vector<thread> v;
	for (int i = 0; i < 5; i++)
	{
		v.push_back(
			thread(
				owner,
				ref(thingsAtHome),
				ref(thingsAvailable),
				ref(distribution),
				ref(generator)
			)
		);
		v.push_back(
			thread(
				thief,
				ref(thingsAtHome),
				ref(thingsAvailable)
			)
		);

	}

	for (auto &t : v)
	{
		t.join();
	}
}