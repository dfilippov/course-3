#pragma once

// Model a situation: there are two people: owner and thief. The former brings
// things to the home, the latter always takes a thing with a maximum value.
void task3();