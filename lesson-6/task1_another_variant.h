#pragma once

// Define a race-condition-safe wrapper for the std::cout object named pcout.
// It should work with several threads and output data to console. Give an
// example of usage.
void task1_another_variant();