#include <thread>
#include "task1_namespace.h"
#include "task1.h"

using namespace std;
using namespace task1_namespace;


// An asynchronous function for the first task. Will be called in separated 
// thread.
template <typename T>
void task1_print(T& out, int number)
{
	out << "Thread #" << number << " started working." << endl;
	this_thread::sleep_for(1s);
	out << "Thread #" << number << " exited." << endl;
}

// TODO: is there a way to detect the thread exit automatically? If yes, 
// remove this custom task1_print() function implementation:
template <>
void task1_print(CoutWrapper& out, int number)
{
	out << "Thread #" << number << " started working." << endl;
	this_thread::sleep_for(1s);
	out << "Thread #" << number << " exited." << endl;
	// Manually calling the end() method in order to unlock the mutex. Ugh...
	out.end();
}


// Define a race-condition-safe wrapper for the std::cout object named pcout.
// It should work with several threads and output data to console. Give an
// example of usage.
void task1()
{
	cout << "Task 1." << endl;
	cout << "Variant with std::cout (race condition):" << endl;
	thread th1rc(task1_print<ostream>, ref(cout), 1);
	thread th2rc(task1_print<ostream>, ref(cout), 2);
	thread th3rc(task1_print<ostream>, ref(cout), 3);
	th1rc.join();
	th2rc.join();
	th3rc.join();
	cout << endl;
	cout << "Variant with pcout (without race condition):" << endl;
	thread th1(task1_print<CoutWrapper>, ref(pcout), 1);
	thread th2(task1_print<CoutWrapper>, ref(pcout), 2);
	thread th3(task1_print<CoutWrapper>, ref(pcout), 3);
	th1.join();
	th2.join();
	th3.join();
}
