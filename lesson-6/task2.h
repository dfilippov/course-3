#pragma once

// Define a function, which calculates the i-th prime number. The calculations
// should take place in a separate thread. Output the progress of calculation
// to console.
void task2();