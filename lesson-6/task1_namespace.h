#pragma once
#include <ios>
#include <iostream>
#include <thread>
#include <mutex>

namespace task1_namespace
{
	class CoutWrapper
	{
	private:
		std::recursive_mutex coutMutex;
		int locksNumber = 0;

		void lock() {
			coutMutex.lock();
			locksNumber++;
		}
	public:
		void end()
		{
			while (locksNumber > 0)
			{
				locksNumber--;
				coutMutex.unlock();
			}
		}

		template <typename T>
		friend CoutWrapper& operator<<(CoutWrapper &self, const T &data)
		{
			self.lock();
			std::cout << data;
			return self;
		}

		friend CoutWrapper& operator<<(CoutWrapper& self, std::ostream& (*f)(std::ostream&))
		{
			self.lock();
			f(std::cout);
			return self;
		}
	};

	CoutWrapper pcout;
}