﻿#include <iostream>
#include "task1.h"
// #include "task1_another_variant.h"
#include "task2.h"
#include "task3.h"

using namespace std;

int main()
{
	task1();
	cout << endl << endl;
	// task1_another_variant();
	// cout << endl << endl;
	task2();
	cout << endl << endl;
	task3();
	cout << endl << endl;
}