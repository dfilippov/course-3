#include <iostream>
#include <fstream>
#include <memory>
#include "task1.h"
#include "fullname.pb.h"
#include "student.pb.h"
#include "students_groups.pb.h"

void task1()
{
	using std::cout;
	using std::endl;

	cout << "Task 1." << endl;

	task1_namespace::FullName fullname;
	fullname.set_name("Vasya");
	fullname.set_surname("Pupkin");

	std::ofstream out("fullname.bin", std::ios_base::binary);
	fullname.SerializeToOstream(&out);
	cout << "task1::FullName.surname() example: " << fullname.surname() << endl;

	task1_namespace::Student student;
	task1_namespace::StudentsGroup group;
}